﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour {

	public float speed = 5.0f;
	public float NPCDistanceDetection = 10.0f;
	public Canvas dialogBox;
	public float dialogBoxTime = 3f;
	private float dialogBoxTimeLeft;
	public int dialogVelocity; // letters per second
	private string currentSentence;
	public Inventory inventory;
	
	private Rigidbody2D rigidbody2d;
	private TMP_Text textMesh;

	private int progress = 0;
	private int nAction = 0;

	private Vector2 direction;
	private Vector2 lookingDirection;


	//ANIMATION MIERDA//
	public float bugAnimationDistance = 14.4f;
	float horizontal;
	int horizontalDirection = 1;
	Animator animator;
	//----------------//

	void Start() {
		rigidbody2d = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		
		dialogBox = Instantiate(dialogBox, new Vector3(0, 0, 0), Quaternion.identity);
		dialogBox.enabled = false;

		foreach (Transform child in dialogBox.transform) {
			if (child.name == "Text") {
				textMesh = child.gameObject.GetComponent<TMP_Text>();
			}
		}

		inventory = Instantiate(inventory, Vector3.zero, Quaternion.identity);
	}

	void Update() {
		
		this.setDirections();
		this.move();
		this.animate();

		this.lookForNPC();
		this.updateDialogBox();
		

	}

	private void animate() {

		horizontal = Input.GetAxis("Horizontal");

		animator.SetFloat("Look X", horizontalDirection);
		animator.SetFloat("Speed", new Vector2(horizontal, 0).magnitude);


		if (horizontal != 0 && Mathf.Sign(horizontal) != horizontalDirection) {
			switch (horizontalDirection) {
				case -1:
					this.transform.position = new Vector3(this.transform.position.x - bugAnimationDistance, this.transform.position.y, 0);
					break;
				case 1:
					this.transform.position = new Vector3(this.transform.position.x + bugAnimationDistance, this.transform.position.y, 0);
					break;
				default:
					Debug.Log("Algo raro ha passat en Player.animate()");
					break;
			}
		}

		if (horizontal != 0)
		{
			horizontalDirection = (int) Mathf.Sign(horizontal);
		}
	}

	private void setDirections() { // Get input and set direction and lookingDirection
		float horizontalInput = Input.GetAxis("Horizontal");
		float verticalInput = Input.GetAxis("Vertical");
				
		direction = new Vector2(horizontalInput, verticalInput);

		if(!Mathf.Approximately(direction.x, 0.0f) || !Mathf.Approximately(direction.y, 0.0f)) {
			lookingDirection = direction;
			lookingDirection.Normalize();
		}

	}


	private void lookForNPC() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			RaycastHit2D ray = Physics2D.Raycast(rigidbody2d.position, lookingDirection, NPCDistanceDetection, LayerMask.GetMask("NPC"));
			if (ray.collider != null) {
				NPC npc = ray.collider.GetComponent<NPC>();
				if (npc != null) {
					this.Talk(npc, progress);
				}
			}
		}
	}


	private void move() { // Player movement
		Vector2 position = rigidbody2d.position;
		position = position + (direction * speed * Time.deltaTime);
		rigidbody2d.MovePosition(position);
	}

	private void Talk(NPC npc, int progress) {

		if (!this.checkAction(npc, progress)) { // Si no hay action, entonces conversa.
			this.converse(npc.GetSentence(progress));
		}

	}

	private void converse(string sentence) {
		dialogBox.enabled = true;
		dialogBoxTimeLeft = dialogBoxTime;
		currentSentence = sentence;
	}

	private bool checkAction(NPC npc, int progress) {
		
		if (npc.actions.Count > 0 && npc.actions[progress] != null) {
			foreach(GAction action in npc.actions[progress]) {
				if (action.nAction == nAction) {
					this.doAction(action, npc);
					return true;
				}
			}

		}
		return false;
	}

	private void doAction(GAction action, NPC npc) {
		switch(action.key) {
			case "talk":
				this.converse(action.sentence);
				break;
			case "give":
				this.giveMaterial(action, npc);
				this.converse(action.sentence);
				break;
			case "recieve":
				if (this.recieveMaterial(action, npc)) {
					this.converse(action.sentence);
				}
				break;
			default:
				Debug.Log("La key no es válida");
				break;
		}
		nAction++;
	}


	private void giveMaterial(GAction action, NPC npc) {

		MaterialController materialController = GameObject.FindWithTag("materialController").GetComponent<MaterialController>();

		GMaterial material = materialController.GetMaterial(action.material);

		if (material == null) {
			Debug.Log("ERROR, La string del material no es correcta (o falta el objeto materialController en la escena)");
			return;
		}

		Instantiate(material.gameObject, npc.gameObject.transform.position, Quaternion.identity);
		material.amount = action.amount;
		material.gameObject.transform.localScale = new Vector3(materialController.onDropScale, materialController.onDropScale, 1f);
		material.gameObject.layer = 11;
		material.gameObject.tag = "material";

	}

	private bool recieveMaterial(GAction action, NPC npc) {

		MaterialController materialController = GameObject.FindWithTag("materialController").GetComponent<MaterialController>();

		GMaterial material = materialController.GetMaterial(action.material);

		GMaterial materialToDonate = null;


		foreach (GMaterial myMaterial in inventory.materials) {
			if (myMaterial.id == material.id) {
				if (myMaterial.amount >= action.amount) {
					materialToDonate = myMaterial;
					break;
				} else {
					this.converse("Parece que no tienes suficientes unidades de " + action.material + ", necesito " + (action.amount - myMaterial.amount) + " más.");
					return false;
				}
			}
		}

		if (materialToDonate == null) {
			return false;
		}

		
		materialToDonate.Stack(-action.amount);



		return true;
	}

	private void updateDialogBox() {
		if (dialogBox.enabled) {

			int lettersDisplayed = (int) (dialogVelocity * (dialogBoxTime - dialogBoxTimeLeft));
			lettersDisplayed = Mathf.Min(lettersDisplayed, currentSentence.Length);
			textMesh.text = currentSentence.Substring(0, lettersDisplayed);

			dialogBoxTimeLeft -= Time.deltaTime;
			if (dialogBoxTimeLeft <= 0) {
				dialogBox.enabled = false;
			}
		}
	}



	private void OnTriggerEnter2D(Collider2D collider) { // recolectingMaterials
		Debug.Log(collider.gameObject.transform.parent.name);
		string type = collider.gameObject.transform.parent.name;
		int i = int.Parse(collider.name);
		
		MaterialController materialController = collider.gameObject.transform.parent.gameObject.transform.parent.GetComponent<MaterialController>();

		//int collectedMaterial = materialController.Collect(type, i);
		//int materialID = materialController.GetMaterialID(type);
	
		GMaterial material = materialController.GetCollectedMaterial("oro");
		material = Instantiate(material, collider.transform.position, Quaternion.identity);
		material.gameObject.transform.localScale = new Vector3(materialController.onDropScale, materialController.onDropScale, 1f);
		material.amount = materialController.GetAmount(type, i);
		//material.SetMask("material");
	}

	public void ClickOnInventoryButton() {
		this.inventory.TriggerInventory();
	}
}
