﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour {


	public GameObject materialList;
	public int regenerationTime = 10; // Cada regenerationTime segundos se produce 1 de material.
	public float onDropScale = 1; // Escala de los objetos minados

	private float[][] lastTimeColected;
	private GMaterial[] materials;

	private Dictionary<string, GameObject> materialIDs;


	void Start() {

		materialIDs = new Dictionary<string, GameObject>();
		foreach(Transform child in materialList.transform) {
			materialIDs.Add(child.name, child.gameObject);
		}


		//lastTimeColected = new float [transform.childCount][]; // Si están todos los tipos de materiales --> Si están bien ordenados también se puede usar
		lastTimeColected = new float [materialIDs.Count][]; // Si NO están todos los tipos de materiales

		foreach(Transform type in transform) {
			
			int materialID = getMaterialID(type.name);
			lastTimeColected[materialID] = new float[type.transform.childCount];
			
			foreach(Transform num in type.transform) {
				int i = int.Parse(num.name);
				lastTimeColected[materialID][i] = 0f;
			}
		}
	}

	void Update() {
		
	}

	public int GetAmount(string type, int i) {

		float elapsedTime = Time.time - lastTimeColected[getMaterialID(type)][i];
		float nonUsedTime = elapsedTime % regenerationTime;
		
		lastTimeColected[getMaterialID(type)][i] = Time.time - nonUsedTime;
		
		return (int) elapsedTime / regenerationTime;
	}

	private int getMaterialID(string type) {
		return materialIDs[type].GetComponent<GMaterial>().id;
	}

	public GMaterial GetMaterial(string type) {
		return materialIDs[type].GetComponent<GMaterial>();
	}

	public GameObject GetCollectedGameObject(string type) {	
		return materialIDs[type];
	}

	public GMaterial GetCollectedMaterial(string type) {	
		return materialIDs[type].GetComponent<GMaterial>();
	}

}