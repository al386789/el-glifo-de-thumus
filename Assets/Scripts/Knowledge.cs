﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Knowledge {

	public string name;
	
	private int lastProgress = -1;

	//public int[] sentencesInEachProgressLevel; // Unity does not supor serialization of multidimensional arrays
	public int sentencesInEachProgressLevel;

	[TextArea(3, 10)]
	public string[] sentences; // First sentence when new progress is reached
	
	[TextArea(3, 10)]
	public string[] otherSentences; // Sentences displayed when the main sentence has been diplayed previously (progress == lastProgress)


	public string GetSentence(int progress) {
		if (progress > lastProgress) {
			lastProgress = progress;
			return sentences[progress];
		}
		return this.getRandomOtherSentence(progress);
	}

	private string getRandomOtherSentence(int progress) {
		return otherSentences[progress * sentencesInEachProgressLevel + Random.Range(0, sentencesInEachProgressLevel)];
	}
}