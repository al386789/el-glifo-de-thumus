﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftController : MonoBehaviour {

	public int numberOfGaps; // Número de huecos que tiene la mesa de crafteo. Cambiar desde la interfaz de unity!! Se pueden contar los transform children

	private Vector3 mousePosition;
	private GMaterial clickedMaterial;
	private GMaterial[] gaps; // Se almacenan los materiales
	private GObject[] objects;
	private GObject craftedObject;

	void Start() {
		gaps = new GMaterial[numberOfGaps];

		GameObject[] gObjects = GameObject.FindGameObjectsWithTag("object");
		objects = new GObject[gObjects.Length];
		for (int i = 0; i < gObjects.Length; i++) {
			objects[i] = gObjects[i].GetComponent<GObject>();
			objects[i].gameObject.SetActive(false);
		}
	}


	void Update() {
		mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		this.manageInput();

		this.dragAndDropMaterial();
	}

	private void manageInput() {
		if (Input.GetMouseButtonDown(0)) { // Left click
			if (clickedMaterial == null) {
				clickedMaterial = this.getClickedMaterial();
			}
		}
		if (Input.GetMouseButtonDown(1)) { // Right click
			removeMaterialFromGap();
		}
	}

	private BoxCollider2D getGap() {
		RaycastHit2D ray = Physics2D.Raycast(mousePosition, Vector2.zero, Mathf.Infinity, LayerMask.GetMask("gap"));
		if (ray.collider != null) { // Mouse está encima de un gap
			BoxCollider2D gap = ray.collider.GetComponent<BoxCollider2D>();
			return gap;
		}
		return null;
	}

	private int getNGap(BoxCollider2D gap) {
		return int.Parse(gap.name.Substring(gap.name.Length - 1));
	}


	private void dragAndDropMaterial() {
		if (clickedMaterial != null) {
			if (Input.GetMouseButton(0)) { // Left button down
				this.dragMaterial();
			} else {
				this.dropMaterial();
			}
		}
	}


	private void dragMaterial() {
		clickedMaterial.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0f);
	}

	private void dropMaterial() {
		BoxCollider2D gap = this.getGap();
		if (gap != null) {
			insertMaterialInGap(clickedMaterial, gap);
		} else {
			Destroy(clickedMaterial.gameObject);
		}
	}


	private void insertMaterialInGap(GMaterial material, BoxCollider2D gap) {
		int nGap = this.getNGap(gap);
		if (gaps[nGap] != null) {
			Destroy(gaps[nGap].gameObject);
		}
		gaps[nGap] = material;
		clickedMaterial = null;

		gaps[nGap].gameObject.transform.position = gap.gameObject.transform.position;

		this.checkCraft();
	}


	private void removeMaterialFromGap() {
		BoxCollider2D gap = getGap();
		if (gap != null) {
			int nGap = getNGap(gap);
			Destroy(gaps[nGap].gameObject);
			gaps[nGap] = null; // #Redundant
			this.checkCraft();
		}
	}

	private GMaterial getClickedMaterial() {

		RaycastHit2D ray = Physics2D.Raycast(mousePosition, Vector2.zero, Mathf.Infinity, LayerMask.GetMask("material"));
		if (ray.collider != null) {
			GMaterial material = ray.collider.GetComponent<GMaterial>();
			if (material != null) {
				GMaterial instance = Instantiate(material, material.transform.position, Quaternion.identity); 
				return instance;
			}
		}
		return null;
	}


	private void checkCraft() {
		string craft = this.getCraft(); // gets current craft
		
		foreach (GObject o in objects) { // compare current craft with all object crafts
			if (craft == o.data.craft) {
				showCraft(o);
				return;
			}
		}
		if (craftedObject != null) {
			craftedObject.gameObject.SetActive(false);
			craftedObject = null;
		}
	}

	private string getCraft() {
		string craft = "";
		foreach (GMaterial material in gaps) {
			if (material != null) {
				craft += material.id.ToString();
			} else {
				craft += "0";
			}
		}
		return craft;
	}

	private void showCraft(GObject o) {
		craftedObject = o;
		craftedObject.gameObject.SetActive(true);
	}
}