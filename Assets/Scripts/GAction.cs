﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GAction {

	public string key;
	public int progressLevel;
	public int nAction;
	
	public string material;
	public int amount;

	public int objectID;

	public Puzzle puzzle;

	[TextArea(3, 10)]
	public string sentence;
}