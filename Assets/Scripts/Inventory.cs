﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {
	
	public GMaterial[] materials;
	private GObject[] objects;

	private GameObject selectedItem;
	private GameObject selectedItemRender;
	//private Vector3 mousePosition;

	private Canvas myCanvas;
	
	void Start() {


		Transform materialGapsT = null;
		Transform objectGapsT = null;

		foreach(Transform child in transform.GetChild(0)) { // Per el fondo
			if (child.name == "materialGaps") {
				materialGapsT = child;
			} else if (child.name == "objectGaps") {
				objectGapsT = child;
			}
		}


		materials = new GMaterial[materialGapsT.childCount];
		foreach (Transform gapT in materialGapsT) {			
			GMaterial voidGM = gapT.gameObject.AddComponent(typeof(GMaterial)) as GMaterial;
			materials[int.Parse(gapT.name)] = voidGM;
		}

		myCanvas = this.gameObject.GetComponent<Canvas>();



		
		selectedItemRender = this.gameObject.transform.GetChild(1).gameObject;
		selectedItemRender.SetActive(false);

		this.CloseInventory();

	}

	


	void Update() {
		//mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		if (myCanvas.enabled) {
			this.manageInput();
			this.moveItems();
		}
	}

	private void moveItems() {
		if (selectedItem != null) {
			selectedItemRender.transform.position = Input.mousePosition;//new Vector3(mousePosition.x, mousePosition.y, 0f);
		}
	}

	private void manageInput() {
		if (Input.GetMouseButtonDown(0)) { // Left click
			if (selectedItem != null) { // Item selected
				this.dropItem();
			} else {

			}
		}
	}

	public void AddMaterial(GMaterial material) {

		// Looking same material
		for (int i = 0; i < materials.Length; i++) {
			if (materials[i].id == material.id) {
				materials[i].Stack(material);
				return;
			}
		}

		// Looking empty gaps
		for (int i = 0; i < materials.Length; i++) {
			if (materials[i].id == 0) { // void
				this.insertMaterial(material, i);
				return;
			}
		}

		// All gaps full
		this.OpenInventory();
		selectItem(material.gameObject);

	}

	private void insertMaterial(GMaterial material, int nGap) {
		
		Sprite materialSprite = material.gameObject.GetComponent<SpriteRenderer>().sprite;
		this.materials[nGap].gameObject.GetComponent<Image>().sprite = materialSprite;

		materials[nGap].Copy(material);
	}

	private void selectItem(GameObject item) {

		selectedItemRender.SetActive(true);

		selectedItem = item;
		
		Sprite itemSprite = item.GetComponent<SpriteRenderer>().sprite;
		selectedItemRender.GetComponent<Image>().sprite = itemSprite;
	}

	private void dropItem() {
		GameObject gap  = getClickedGap();
		if (gap == null) {
			Debug.Log("Out bro");
			return; // #C 
		}

		bool isMaterial = false;
		if (selectedItem.GetComponent<GMaterial>() != null) {
			isMaterial = true;
		}

		if (gap.transform.parent.name == "materialGaps" && isMaterial) {
			Debug.Log("in material");
			insertMaterial(selectedItem.GetComponent<GMaterial>(), int.Parse(gap.name));
			selectedItem = null;
			selectedItemRender.SetActive(false);
		} else if (gap.transform.parent.name == "objectGaps" && !isMaterial) {
			Debug.Log("in object");
			selectedItem = null;
			selectedItemRender.SetActive(false);
		}
	}

	public void OpenInventory() {
		this.myCanvas.enabled = true;
	}

	public void CloseInventory() {
		this.myCanvas.enabled = false;
	}

	public void TriggerInventory() {
		this.myCanvas.enabled = !this.myCanvas.enabled;
	}

	private GameObject getClickedGap() {
		RaycastHit2D ray = Physics2D.Raycast(Input.mousePosition, Vector2.zero, Mathf.Infinity, LayerMask.GetMask("clickable"));
		if (ray.collider != null) {
			GameObject gap = ray.collider.gameObject;
			return gap;
		}
		return null;
	}
}
