﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickController : MonoBehaviour {

	public string sceneChangerKey = "_SC";
	public int size = 100;
	private Player player;
    public Texture2D cursorClickImg, cursorSensibleImg, cursorImg;
	public CursorMode cursorMode = CursorMode.Auto;

	void Start() {
		setCursor(0);
		player = GameObject.FindWithTag("Player").GetComponent<Player>();
	}

    void Update() {
		this.manageInput();
		this.manageCursor();
	}

	private void manageInput() {
		if (Input.GetMouseButtonDown(0)) { // Left click
			this.checkClick();
		}
		// if (Input.GetMouseButtonDown(1)) { // Right click
		// }
	}

	private void checkClick() {

		Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		RaycastHit2D ray = Physics2D.Raycast(mousePosition, Vector2.zero, Mathf.Infinity, LayerMask.GetMask("clickable"));
		if (ray.collider != null) {

			string tag = ray.collider.gameObject.tag;

			switch (tag) {
				case "sceneChanger":
					this.lookForScene(ray.collider.name);
					break;
				case "material":
					this.clickOnMaterial(ray.collider.gameObject.GetComponent<GMaterial>());
					break;
				case "object":
					
					break;
				// case "HUD":
				// 	this.clickOnHUD();
				// 	break;
				default:
					Debug.Log("ERROR?, El objeto clickado no tiene un tag válido asignado!!");
					break;
			}
			
			
		}
	}

	private void lookForScene(string objectName) {

		int posOfKey = objectName.IndexOf(sceneChangerKey);
	
		if (posOfKey != -1) { // The key exists in the string
			SceneManager.LoadScene("Scenes/" + objectName.Substring(posOfKey + sceneChangerKey.Length));
		}
	}


	private void manageCursor() {


		if (Input.GetMouseButtonDown(0)) {
			setCursor(1);
		} else if (Input.GetMouseButtonUp(0)) {
			setCursor(0);
		}

		// if (Input.GetMouseButtonDown(0)) setCursor(2);
		// else if (overSensitive) setCursor(1);
		// else setCursor(0);
	}

    public void setCursor(int tipoCursor){

		switch(tipoCursor) {
			case 0:
				Cursor.SetCursor(cursorImg, Vector2.zero, cursorMode);
				break;
			case 1:
				Cursor.SetCursor(cursorSensibleImg, Vector2.zero, cursorMode);
				break;
			case 2:
				Cursor.SetCursor(cursorClickImg, Vector2.zero, cursorMode);
				break;
			default:
				Cursor.SetCursor(cursorImg, Vector2.zero, cursorMode);
				break;
		}
    }
	private void clickOnMaterial(GMaterial material) {

		player.inventory.AddMaterial(material);
		Destroy(material.gameObject);

		//player.inventory.AddMaterial(Instantiate(material, material.transform.position, Quaternion.identity));
		//Destroy(material);

	}

	// private void clickOnHUD(string name) {

	// 	switch(name) {
	// 		case "inventoryButton":
	// 			player.inventory.OpenInventory();
	// 		default:
	// 			Debug.Log("Ese nombre no tiene ninguna función asignada.");
	// 			break;
	// 	}
	// }

}


