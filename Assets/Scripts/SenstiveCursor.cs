﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SenstiveCursor : MonoBehaviour {
	public ClickController clickController;
	
	void OnMouseEnter() {
		clickController.setCursor(1);
	}

	void OnMouseExit() {
		clickController.setCursor(0);
	}
}
