﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImgPuzzleContoller : MonoBehaviour {

	private Vector3 mousePosition;
	private Vector3 previousMouseposition;
	private int selectedQuad = -1;
	private Vector2 selectedQuadPos;
	private Puzzle currentPuzzle;
	private int[,] puzzleState;
	private bool movingPiece = false;
	private Vector2 movingPieceAxis;

	private GameObject meshObject;
	private Mesh mesh;


	void Start() {

		currentPuzzle = this.getPuzzle("glifo"); // #C "glifo" --> global variable with the name of the puzzle (from static class)
		if (currentPuzzle == null) {
			Debug.Log("ERROR: Puzzle no encontrado (ImgPuzzleController --> Start())");
			return;
		}

		puzzleState = new int[currentPuzzle.n, currentPuzzle.n];

		this.generatePuzzle(currentPuzzle);
		this.shufflePuzzle(currentPuzzle.n);
	}

	void Update() {
		mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		this.manageInput();

		//print(puzzleState); // #Debug

	}

	private void manageInput() {
		if (Input.GetMouseButtonDown(0)) { // Left click
			previousMouseposition = mousePosition;
			Texture texture = currentPuzzle.material.mainTexture;
			
			// Click inside image
			if (mousePosition.x > meshObject.transform.position.x && mousePosition.x < meshObject.transform.position.x + texture.width &&
				mousePosition.y > meshObject.transform.position.y && mousePosition.y < meshObject.transform.position.y + texture.height) {

				selectedQuadPos = this.getPiecePos(mousePosition, texture);
				selectedQuad = puzzleState[(int) selectedQuadPos.x, (int) selectedQuadPos.y];				
				puzzleState[(int) selectedQuadPos.x, (int) selectedQuadPos.y] = -1;
			}
		}
		if (Input.GetMouseButtonDown(1)) { // Right click

		}
		if (Input.GetMouseButtonUp(0)) { // Left click up
			this.dropPiece();
		}

		if (Input.GetMouseButton(0)) { // Left button down
			this.dragPiece();
			previousMouseposition = mousePosition;
		}
	}

	private Vector2 getPiecePos(Vector3 clickPosition, Texture texture) {
		if (clickPosition.x > meshObject.transform.position.x && clickPosition.x < meshObject.transform.position.x + texture.width &&
				clickPosition.y > meshObject.transform.position.y && clickPosition.y < meshObject.transform.position.y + texture.height) {

			Vector2 rClickPosition = new Vector2(((texture.width / 2) + clickPosition.x) / texture.width, 1 - (((texture.height / 2) + clickPosition.y) / texture.height));
			int rQuadPosition = (int) (Mathf.Floor(rClickPosition.x * (currentPuzzle.n - 1) + 0.5f) + (Mathf.Floor(rClickPosition.y * (currentPuzzle.n - 1) + 0.5f) * currentPuzzle.n));
			int i = (int) rQuadPosition / currentPuzzle.n;
			int j = (int) (rQuadPosition % currentPuzzle.n);
			return new Vector2(i, j);
		}
		return new Vector2(-1, -1);
	}

	private void dragPiece() {
		if (selectedQuad != -1) {
			Vector2 mouseDirection2D = this.getMouseMovementDirection2D(mousePosition, previousMouseposition);
			if (canMove(selectedQuadPos, mouseDirection2D)) {
				movingPiece = true;
				movingPieceAxis = new Vector2(Mathf.Pow(mouseDirection2D.x, 2), Mathf.Pow(mouseDirection2D.y, 2)); // Positive

				Vector3 originQuadPosition = new Vector3(selectedQuadPos.y * currentPuzzle.side, ((currentPuzzle.n - 1) - selectedQuadPos.x) * currentPuzzle.side, 0);
				Vector3 quadPosition = mesh.vertices[selectedQuad * 4];
				float increment = ((mousePosition - previousMouseposition) * movingPieceAxis).magnitude;
				float maxIncrement = (originQuadPosition + ((Vector3) mouseDirection2D * currentPuzzle.side) - quadPosition).magnitude;
				float finalIncrement = Mathf.Min(maxIncrement, increment);

				this.movePieceTo(selectedQuad, quadPosition + (Vector3) mouseDirection2D * finalIncrement);

				if (maxIncrement == 0) {
					Vector2 dropPos = selectedQuadPos + Vector2.Perpendicular(mouseDirection2D);
					puzzleState[(int) dropPos.x, (int) dropPos.y] = selectedQuad;

					movingPiece = false;
					movingPieceAxis = Vector2.zero;
					selectedQuad = -1;
					selectedQuadPos = new Vector2(-1, -1);
					this.checkSolution();
				}
			}		
		}
	}

	private bool canMove(Vector2 quadPos, Vector2 direction) {

		Vector2 pointingPos = quadPos + Vector2.Perpendicular(direction); // BOOM
		if (pointingPos.x < 0 || pointingPos.x > currentPuzzle.n - 1 || pointingPos.y < 0 || pointingPos.y > currentPuzzle.n - 1){
			return false;
		} else if (puzzleState[(int) pointingPos.x, (int) pointingPos.y] == -1) {
			return true;
		}
		return false;
	}

	private void movePiece(int nQuad, Vector3 increment) {
		Vector3[] vertices = mesh.vertices;

		for(int i = 0; i < 4; i++) { // Each quad have 4 vertices
			vertices[nQuad * 4 + i] = vertices[nQuad * 4 + i] + increment;
		}

		mesh.vertices = vertices;
	}

	private void movePieceTo(int nQuad, Vector3 newPosition) {
		Vector3[] vertices = mesh.vertices;

		Vector3 increment = newPosition - vertices[nQuad * 4];

		for(int i = 0; i < 4; i++) { // Each quad have 4 vertices
			vertices[nQuad * 4 + i] = vertices[nQuad * 4 + i] + increment;
		}

		mesh.vertices = vertices;
	}

	private void dropPiece() {

		if (movingPiece) {
			movingPiece = false;

			
			Texture texture = currentPuzzle.material.mainTexture;

			Vector2 dropPos = this.getPiecePos(mousePosition, texture);

			Vector3[] vertices = (Vector3[]) mesh.vertices;

			if (dropPos.x == -1 || puzzleState[(int) dropPos.x, (int) dropPos.y] != -1) { // Opio ... (pero no estan implementades les "colisions")
				this.movePiece(selectedQuad, new Vector3(selectedQuadPos.y * currentPuzzle.side, ((currentPuzzle.n - 1) - selectedQuadPos.x) * currentPuzzle.side, 0) - vertices[selectedQuad * 4]);
				puzzleState[(int) selectedQuadPos.x, (int) selectedQuadPos.y] = selectedQuad;
			} else {
				this.movePiece(selectedQuad, new Vector3(dropPos.y * currentPuzzle.side, ((currentPuzzle.n - 1) - dropPos.x) * currentPuzzle.side, 0) - vertices[selectedQuad * 4]);
				puzzleState[(int) dropPos.x, (int) dropPos.y] = selectedQuad;
			}

			movingPieceAxis = Vector2.zero;
			selectedQuad = -1;
			selectedQuadPos = new Vector2(-1, -1);

		} else if (selectedQuadPos.x != -1) {
			puzzleState[(int) selectedQuadPos.x, (int) selectedQuadPos.y] = selectedQuad;
			selectedQuad = -1;
			selectedQuadPos = Vector2.zero;
		}

		this.checkSolution();
	}

	private void makeQuadOfImage(Vector3 origin, float side, Vector3 auxInfo, Vector3[] vertices, int[] triangles, Vector2[] uv, int nQuad) { // Works if we ONLY insert quads, BE CAREFUL
		// Each quad --> 4 vertices, 6 edges and 4 uv.
		// auxInfo = (j, i, n)

		vertices[nQuad * 4] = origin;
		vertices[nQuad * 4 + 1] = new Vector3(origin.x, origin.y + side, 0);
		vertices[nQuad * 4 + 2] = new Vector3(origin.x + side, origin.y + side, 0);
		vertices[nQuad * 4 + 3] = new Vector3(origin.x + side, origin.y, 0);

		triangles[nQuad * 6] = nQuad * 4;
		triangles[nQuad * 6 + 1] = nQuad * 4 + 1;
		triangles[nQuad * 6 + 2] = nQuad * 4 + 2;
		triangles[nQuad * 6 + 3] = nQuad * 4 + 0;
		triangles[nQuad * 6 + 4] = nQuad * 4 + 2;
		triangles[nQuad * 6 + 5] = nQuad * 4 + 3;

		float rSide = 1 / auxInfo.z; // 1 / n

		uv[nQuad * 4] = new Vector2(rSide * auxInfo.x, rSide * auxInfo.y);
		uv[nQuad * 4 + 1] = new Vector2(rSide * auxInfo.x, rSide * (auxInfo.y + 1));
		uv[nQuad * 4 + 2] = new Vector2(rSide * (auxInfo.x + 1), rSide * (auxInfo.y + 1));
		uv[nQuad * 4 + 3] = new Vector2(rSide * (auxInfo.x + 1), rSide * auxInfo.y);

	}

	private void generatePuzzle(Puzzle puzzle) {
		
		int width = puzzle.material.mainTexture.width;
		int height = puzzle.material.mainTexture.height;

		if (width != height || width % puzzle.n != 0) {
			Debug.Log("ERROR: La imagen no es cuadrada o no es divisible entre " + puzzle.n);
			return;
		}

		int numQuads = (int) Mathf.Pow(puzzle.n, 2);

		Vector3[] vertices = new Vector3[numQuads * 4];
		Vector2[] uv = new Vector2[numQuads * 4];
		int[] triangles = new int[numQuads * 6];

		currentPuzzle.side = (int) width / puzzle.n;

		int nQuad = 0;
		for (int i = puzzle.n - 1; i >= 0; i--) {
			for (int j = 0; j < puzzle.n; j++) {
				this.makeQuadOfImage(new Vector3(currentPuzzle.side * j, currentPuzzle.side * i, 0), currentPuzzle.side, new Vector3(j, i, puzzle.n), vertices, triangles, uv, nQuad);
				nQuad++;
			}
		}

		mesh = new Mesh();

		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.triangles = triangles;

		meshObject = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer));
		//gameObject.transform.localScale = new Vector3(0.04f, 0.04f, 1);
		meshObject.transform.position = new Vector3(-width / 2, -height / 2, 0);

		meshObject.GetComponent<MeshFilter>().mesh = mesh;

		meshObject.GetComponent<MeshRenderer>().material = puzzle.material;
	}

	private Puzzle getPuzzle(string puzzleName) {
		foreach (Transform child in transform) {
			Puzzle p = child.GetComponent<Puzzle>();
			if (puzzleName == p.pName) {
				return p;
			}
		}
		return null;
	}

	private int[] shuffleVector(int vLength) { // Knuth shuffle algorithm, O(2 * vLength)
		int[] v = new int[vLength];

		for (int i = 0; i < vLength; i++) {
			v[i] = i;
		}

		for (int i = 0; i < vLength; i++) {
			int aux = v[i];
			int r = (int) Random.Range(i, vLength);
			v[i] = v[r];
			v[r] = aux;
		}

		return v;

	}

	private void shufflePuzzle(int n) {
		int[] randomVector = this.shuffleVector((int) Mathf.Pow(n, 2));
		this.setPuzzleState(randomVector);


		Vector3[] originalVertices = (Vector3[]) mesh.vertices.Clone();

		for (int nQuad = 0; nQuad < Mathf.Pow(n, 2); nQuad++) {
			this.movePiece(randomVector[nQuad], originalVertices[nQuad * 4] - originalVertices[randomVector[nQuad] * 4]);
		}

		Vector2 quadToDeletePos = new Vector2(Random.Range(0, currentPuzzle.n), Random.Range(0, currentPuzzle.n));
		currentPuzzle.deletedQuad = puzzleState[(int) quadToDeletePos.x, (int) quadToDeletePos.y];
		puzzleState[(int) quadToDeletePos.x, (int) quadToDeletePos.y] = -1;

		this.movePiece(currentPuzzle.deletedQuad, -mesh.vertices[currentPuzzle.deletedQuad * 4] - new Vector3(3 * currentPuzzle.side, 0, 0));
	}

	private void setPuzzleState(int[] randomized) {
		for (int i = 0; i < currentPuzzle.n; i++) {
			for (int j = 0; j < currentPuzzle.n; j++) {
				int quad = randomized[i * currentPuzzle.n + j];
				puzzleState[i, j] = quad;
				//puzzleState[i, j] = i * currentPuzzle.n + j; // #Testing
			}
		}

		//puzzleState[(int) randomized[randomized.Length - 1] / currentPuzzle.n, (int) randomized[randomized.Length - 1] % currentPuzzle.n] = 0;
	}

	private Vector2 getMouseMovementDirection2D(Vector3 mousePosition, Vector3 previousMouseposition) {
		Vector3 direction = (mousePosition - previousMouseposition);

		if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) {
			return new Vector2(1, 0) * Mathf.Sign(direction.x);
		} else {
			return new Vector2(0, 1) * Mathf.Sign(direction.y);
		}
	}

	private bool checkSolution() {
		int nQuad = 0;
		for (int i = 0; i < currentPuzzle.n; i++) {
			for (int j = 0; j < currentPuzzle.n; j++) {
				int q = puzzleState[i, j];
				if (q != nQuad && q != -1) {
					return false;
				}
				nQuad++;
			}
		}
		Debug.Log("PUZZLE RESUELTO!!");
		return true;
	}

	/* // #Debug
	private void print(int[,] m) {
		string s = "";
		for (int i = 0; i < m.GetLength(0); i++) {
			for (int j = 0; j < m.GetLength(1); j++) {
				s += m[i,j] + ", ";
			}
			s += "\n";
		}
		Debug.Log(s);
	}
	*/
}




/*
	private Vector3[] getQuadVertices(int nQuad, Vector3[] vertices) {
		Vector3[] quadVertices = new Vector3[4];
		for (int i = 0; i < 4; i++) {
			quadVertices[i] = vertices[nQuad * 4 + i];
		}
		return quadVertices;
	}


	private void makeQuad(Vector3 origin, float width, float height, Vector3[] vertices, int[] triangles, Vector2[] uv, int nQuad) { // Works if we ONLY insert quads, BE CAREFUL
		// Each quad --> 4 vertices, 6 edges and 4 uv.

		vertices[nQuad * 4] = origin;
		vertices[nQuad * 4 + 1] = new Vector3(origin.x, origin.y + height, 0);
		vertices[nQuad * 4 + 2] = new Vector3(origin.x + width, origin.y + height, 0);
		vertices[nQuad * 4 + 3] = new Vector3(origin.x + width, origin.y, 0);

		triangles[nQuad * 6] = nQuad * 4;
		triangles[nQuad * 6 + 1] = nQuad * 4 + 1;
		triangles[nQuad * 6 + 2] = nQuad * 4 + 2;
		triangles[nQuad * 6 + 3] = nQuad * 4 + 0;
		triangles[nQuad * 6 + 4] = nQuad * 4 + 2;
		triangles[nQuad * 6 + 5] = nQuad * 4 + 3;

		uv[nQuad * 4] = new Vector2(0, 0);
		uv[nQuad * 4 + 1] = new Vector2(0, 1f);
		uv[nQuad * 4 + 2] = new Vector2(1f, 1f);
		uv[nQuad * 4 + 3] = new Vector2(1f, 0);

	}
*/