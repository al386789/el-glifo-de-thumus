﻿// CLASE ESTÁTICA PARA PASAR DATOS ENTRE ESCENAS
// NO SE ACCEDE DIRECTAMENTE A LAS VARIABLES, SE USAN PROPIEDADES (PROGRA II :)

/* EJEMPLO:

Desde cualquier clase, podemos acceder al progress (o nivel) de Abigail con: GameMaster.Progress


GameMaster.Progress = 4;

int progressAbigail = GameMaster.Progress;

Debug.Log(progressAbigail); //--> 4

*/


public static class GameMaster {

	private static int progress;

	public static int Progress {get {return progress;} set {progress = value;}}

}
