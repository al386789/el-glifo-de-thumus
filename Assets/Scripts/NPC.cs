﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	public string myName;
	
	private int lastProgress = -1;

	[TextArea(3, 10)]
	public string[] sentences; // First sentence when new progress is reached
	
	[TextArea(3, 10)]
	public string[] otherSentences; // Sentences displayed when the main sentence has been diplayed previously (progress == lastProgress)
	
	public GAction[] allActions;

	//private GAction[][] actions;
	public List<List<GAction>> actions;

	void Start() {

		actions = new List<List<GAction>>();
		
		foreach(GAction action in allActions) {
			if (action.progressLevel > actions.Count - 1) {
				for (int i = actions.Count; i <= action.progressLevel; i++) {
					actions.Add(new List<GAction>());
				}
			}
			actions[action.progressLevel].Add(action);
		}
	}

	void Update() {
		
	}

	// public string Talk(int progress) {
	// 	return this.getSentence(progress);
	// }

	public string GetSentence(int progress) {
		if (progress > lastProgress) {
			lastProgress = progress;
			return sentences[progress];
		}
		return this.getRandomOtherSentence(progress);
	}

	private string getRandomOtherSentence(int progress) {
		return otherSentences[Random.Range(0, otherSentences.Length)];
	}
}
