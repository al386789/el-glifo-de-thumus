﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMaterial : MonoBehaviour {

	//public MaterialData data;

	public string materialName;
	public int id;
	
	public int amount;

	public void Copy(GMaterial material) {
		this.materialName = material.materialName;
		this.id = material.id;
		this.amount = material.amount;
	}

	public void Stack(GMaterial material) {
		this.amount += material.amount;
	}

	public bool Stack(int amount) {
		
		if (this.amount + amount > 0) {
			this.amount += amount;
			return true;
		} else if (this.amount == amount) {
			Destroy(this.gameObject); // #Provisional
			return true;
		}
		return false;
	}
}
