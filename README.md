# **Basics branch**

# Documentación

> Las variables y funciones privadas no están documentadas excepto algunas excepciones en las que se puede hacer uso de ellas.

## CraftController

**Clase** controlador de la escena para craftear.

Variables :
* numberOfGaps : número de huecos que tiene la 'mesa de crafteo'. Sólo hay que cambiarla (desde la interfaz de Unity) si se cambia el número de huecos. Está porque por ahora, los huecos son provisionales. (int)

Variables privadas :
* mousePosition : posición del ratón **respecto a la pantalla** (Vector3)
* clickedMaterial : material que tenemos seleccionado (que estamos arrastrando), si no tenemos ningún material seleccionado, es null (GMaterial)
* gaps : array con los GMaterial que hay en cada hueco, si no hay material en algún hueco, gaps en esa posición será null (GMaterial[])
* objects : array con TODOS los objetos disponibles en el juego, se inicializa en Start(), sirve para comparar los crafteos de cada objeto con el crafteo que ej jugador hace (GObject[])
* craftedObject : objeto crafteado por el jugador o null si no consigue nada (GObject)

> Para cambiar la forma o número de huecos de la 'mesa de crafteo'.
> 1. Colocar tantos BoxCollider2D como huecos vaya a tener la 'mesa', cuanto más grande sea la hitbox, más área tendremos para soltar el material y que se quede en el sitio. (los BoxCollider2D son los objetos llamados gap que están dentro del objeto pentaculo)
> 2. Es importante que los *gaps* estén dentro del objeto que contiene el script CraftController, en este caso dentro del objeto pentáculo. (En realidad creo que da igual pero, mejor que estén ahí)
> 3. Lo que sí que es importante son los nombres que le demos a estos objetos, se pueden llamar como sea pero han de acabar con un número, que será la posición que tendrá el objeto insertado en ese hueco en la string craft (más info sobre esta string en GObjectData).
> 4. Cambiar la propiedad *numberOfGaps* del objeto con el script *CraftController* al número de huecos que tenemos ahora.

Por ahora la clase sólo soporta 10 huecos, si se pretende poner más de 10, hay que hacer unos pequeños cambios.

---

## GMaterial

**Clase** para los materiales.

Variables :
* data : variable de tipo GMaterialData, contiene los datos del material en específico. (GMaterialData)

---

## GMaterialData

La **clase** GMaterialData guarda la información de los materiales. Es un tipo de dato. (System.Serialization)

Variables :
* name : nombre del material (string)
* id : id del material (int) (obviamente no se pueden repetir)

> Los nombres y IDs de los materiales tienen que estar especificados en el archivo *materials.txt*

---

## GObject

**Clase** para los objetos.

Variables :
* data : variable de tipo GObjectData, contiene los datos del objeto en específico. (GObjectData)

---

## GObjectData

La **clase** GObjectData guarda la información de los objetos. Es un tipo de dato. (System.Serialization)

Variables :
* name : nombre del objeto (string)
* id : id del objeto (int) (obviamente no se pueden repetir)
* craft : string con la información del craft del objeto (string)

> Los nombres, los IDs y los crafts de los objetos tienen que estar especificados en el archivo *objects.txt*.  
Hay ejemplos de cómo generar la string craft en *objects.txt*. (muy fácil)

---

## Player

**Clase** del jugador al que se maneja.

Variables :
* seed : velocidad con la que se mueve el jugador (float)
* NPCDistanceDetection : distancia a la que el jugador puede hablar con los NPCs (float)

---

## NPC

**Clase** de los NPCs.

Variables :
* knowledge : información que tiene el NPC, nombre, diálogos, objetos, bluebrints ... (Knowledge)

Funciones públicas :
* void Talk() : crea un dialogBox con la frase que le toca decir al NPC. (NO IMPLEMENTADA, FALTA DIALOG_BOX_PREFAB)

---

## Knowledge

La **clase** knowledge guarda la información que los npc tienen, diálogos, objetos, etc. Es un tipo de dato.  
Cada npc tiene un elemento de la clase Knowledge, desde el propio editor de unity se puede acceder a las variables (diálogos) (System.Serialization)

Variables :
* lastProgress : último nivel de progreso con el que se ha hablado con el npc (int)
* sentences : lista de diálogos que el npc tendrá con el personaje cuando se alcance el progress adecuado. (string[])
* sentencesInEachProgressLevel : número de *other sentences* que hay para cada nivel (progress). (int)
* otherSentences : lista de diálogos secundarios que el npc tendrá con el personaje, estos solo serán 'soltados' una vez se haya hablado con el personaje y este haya 'soltado' su frase principal o clave. (string[])  

> Ej. Player pasa del nivel (progress) 2 al 3, habla con el Herrero, este 'suelta' su frase principal, en este caso sentence[3], si el personaje vuelve a hablar con el Herreo, este 'soltará'
aleatoriamente una de las frases guardadas en otherSentences para su nivel de progress.

Funciones públicas :
* string GetSentence(int progress) --> devuelve la sentence correspondiente.

---
---

# Convenciones (tocs)

## Lower Camel Case
* Variables
* Funciones privadas


```C#
	public int myVariable = 4;
	private void myFunction(int a) {
		Debug.Log("Hola");
	}
```

## Upper Camel Case
* Funciones públicas

```C#
	public void MyFunction(int a) {
		Debug.Log("Hola");
	}
```